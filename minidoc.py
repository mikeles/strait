#!/usr/bin/env python3
# Author: michele.simionato@gmail.com
"""
minidoc: a minimal documentation utility.

Minidoc automatically extracts documentation from a Python module or
package. The module/package has to be in the current PYTHONPATH.
All the features are encoded in the ``Document`` class, which can be imported
in other modules and fully exploited. Alternatively, minidoc can be used as
a command line tool:

- minidoc package # create index.html
"""
# Notice that (on design) minidoc documents functions, classes and metaclasses
# but not other globals. It is not independend from rst.py, but maybe they
# should be integrated (or maybe not). It should be rewritten from scratch
# since it is difficult to extend :-(

# imports
import sys
import os
import re
import inspect
import textwrap
import importlib
from functools import partial
from docutils.core import publish_cmdline, default_description
DDOLLAR = re.compile(r'(\$\$\$[\w_\d]+)|\$\$([\w_\d]+)')
DOCTEST = re.compile(r'#\s*doctest:.*')
ERR = sys.stderr  # useful alias
PYGMENTS_CSS = ''

# trick to retrieve modules in the current directory but not in PYTHONPATH
sys.path.insert(0, '.')


def getsourcelines(object):
    "A replacement for inspect.getsourcelines understanding decorators"
    if isinstance(object, partial):
        object = object.args[0]
    try:
        object = object.__wrapped__
    except Exception:
        pass
    if isinstance(object, partial):
        object = object.args[0]
    lines, lnum = inspect.findsource(object)
    if inspect.ismodule(object):
        return lines, 0
    else:
        return inspect.getblock(lines[lnum:]), lnum + 1


class MultiReplacer(object):
    '''
    Replace a set of regular expressions in a text using a set of replacing
    methods.
    '''
    def __init__(self):
        self.dic = {}
        rx = []
        for i, name in enumerate(
                n for n in dir(self) if n.startswith('repl_')):
            meth = getattr(self, name)
            regex = meth.__defaults__[0]
            rx.append('(%s)' % regex)
            self.dic[i + 1] = meth
        self.rx = re.compile('|'.join(rx))

    def replacer(self, mo):
        group = mo.group()
        try:
            return self.dic[mo.lastindex](group)
        except Exception:
            print('Error when trying to replace %r' % group)
            raise

    def __call__(self, text):
        return self.rx.sub(self.replacer, text)


def gendoc(mod, codeblock=True):
    """
    Given a module, gets its docstring and replaces <$$object> with the
    source code of <object>.
    """
    def repl(mo):
        if mo.group(2) is None:  # escaped $$$
            return mo.group(1)[1:]
        obj = getattr(mod, mo.group(2))
        if isinstance(obj, str):
            lines = obj.splitlines()
        else:
            try:
                lines = getsourcelines(obj)[0]
            except IOError:
                raise IOError('Cannot get the source code for %r' % obj)
        if codeblock:  # colored output
            return '.. code-block:: python\n\n%s\n' \
                % '\n'.join(' ' + ln.rstrip() for ln in lines)
        return '::\n\n%s\n' % '\n'.join(' ' + ln.rstrip() for ln in lines)
    return DDOLLAR.sub(repl, mod.__doc__)


def splitlonglines(doc, N=131):
    out = []
    for line in doc.splitlines():
        mo = DOCTEST.search(line)
        if mo:  # remove #doctest: directive
            line = DOCTEST.sub('', line)
        if len(line) > N and not line.startswith('..'):
            for ln in textwrap.wrap(line, width=N):
                out.append(ln)
        else:
            out.append(line)
    return '\n'.join(out)


def create_html(modname, dest):
    """Parses the command line arguments and does the right thing."""
    mod = importlib.import_module(modname)
    os.mkdir('public')
    with open(dest + '/index.rst', 'w') as f:
        f.write(splitlonglines(gendoc(mod)))
    opt = ['-s', '-g', '-t', '--traceback']
    publish_cmdline(
        writer_name='html', description=default_description,
        argv=[dest + '/index.rst', dest + '/index.html'] + opt)
    print('Generated', os.listdir(dest))


if __name__ == '__main__':
    create_html(sys.argv[1], 'public')
