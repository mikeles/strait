# strait

I provide a simple implementation of traits as units of composable behavior for Python. I argue that traits are better than multiple inheritance. Implementing frameworks based on traits is left as an exercise for the reader.

# Dependencies

The strait module requires Python 3.4+

# Installation

`pip install strait` or `python setup.py install`

# Tests

You may run the tests from the package directory as follows:

$ python docs.py
